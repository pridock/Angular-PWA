import { Component, OnInit } from '@angular/core';
import * as data from './articles.json';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {

article = {
  title: '',
  id: '',
  content: '',
  source: '',
  author: ''
};
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
      const category = this.activatedRoute.snapshot.paramMap.get('category');
      const article =  +this.activatedRoute.snapshot.paramMap.get('id');
      const cats = <any>data;
      const cat = cats.categories;
      cat.forEach( val => {
        const arts = (val.slug === category) ? val.articles : null;
        if (arts !== null) {
          for (const art of arts) {
            console.log(art.id + ' ' + article);
            if (art.id === article) {
              this.article = art;
              break;
            } else {
              this.article = {
                title: 'Not Found',
                id: '',
                content: '',
                source: '',
                author: ''
              };
            }
          }
        }
      });

  }

}
