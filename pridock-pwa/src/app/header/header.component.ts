import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isNavbarCollapsed = true;
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  openLoginModal(content) {
    this.modalService.open(content);
  }

}
